package fr.ensai.metier.test.type;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.ensai.metier.type.TypeBoolean;

public class TypeBooleanTest {

	@Test
	public void verifyConstraintTrue() {
		TypeBoolean tb = new TypeBoolean();
		
		assertTrue("insertion true",tb.verifyConstraints("true"));
		assertTrue("insertion false",tb.verifyConstraints("false"));
		assertTrue("insertion T",tb.verifyConstraints("T"));
		assertTrue("insertion F",tb.verifyConstraints("F"));
		assertTrue("insertion 1",tb.verifyConstraints("1"));
		assertTrue("insertion 0",tb.verifyConstraints("0"));
		assertTrue("insertion vide",tb.verifyConstraints(""));
	}
	
	@Test
	public void verifyConstraintFalse() {
		TypeBoolean tb = new TypeBoolean();
		
		assertTrue(!tb.verifyConstraints("Tr"));
		assertTrue(!tb.verifyConstraints("fd"));
		assertTrue(!tb.verifyConstraints("10"));
	}
	
	@Test
	public void update() {
		TypeBoolean tb = new TypeBoolean();
		
		assertEquals("true",tb.update("true", "false"));
		assertEquals("true", tb.update("true", "true"));
		assertEquals("false", tb.update("false", "false"));
		assertEquals("T", tb.update("T", "false"));
		assertEquals("", tb.update("", "1"));
	}

}
