package fr.ensai.metier.test.type;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.ensai.metier.type.TypeDouble;

public class TypeDoubleTest {

	private TypeDouble tb;

	@Test
	public void verifyConstraintTrue() {
		tb = new TypeDouble(-100, 100, 2);
		assertTrue("11 verifyTrue", tb.verifyConstraints("11"));
		assertTrue("53.33 verifyTrue", tb.verifyConstraints("53.33"));
		assertTrue("53.3458657789 verifyTrue",
				tb.verifyConstraints("53.3458657789"));
	}

	@Test
	public void verifyConstraintFalse() {
		tb = new TypeDouble(-100, 100, 2);
		assertFalse("101 verifyFalse", tb.verifyConstraints("101"));
		assertFalse("-200 verifyFalse", tb.verifyConstraints("-200"));
		assertFalse("200. verifyFalse", tb.verifyConstraints("200."));
		assertFalse("20vhz0. verifyFalse", tb.verifyConstraints("20vhz0."));
	}

	@Test
	public void update() {
		tb = new TypeDouble(-100, 100, 2);
		assertEquals("98.99 update", String.valueOf(98.99),
				tb.update("98.999", String.valueOf(10)));
		assertEquals("10.10 update", String.valueOf(10.10),
				tb.update("10.10", String.valueOf(10)));
		assertEquals("10 update", String.valueOf(10.0),
				tb.update("10", String.valueOf(10)));
	}

}
