package fr.ensai.metier.test.valuedCell;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import fr.ensai.metier.header.Feature;
import fr.ensai.metier.header.Product;
import fr.ensai.metier.pcm.Pcm;
import fr.ensai.metier.type.TypeDate;
import fr.ensai.metier.type.TypeDouble;
import fr.ensai.metier.valuedCell.ValuedCell;

public class ValuedCellTest {

	private static ValuedCell iValuedCell;
	private static ValuedCell iValuedCell2;
	private static Product product1;
	private static Product product2;
	private static Feature feature1;
	private static Feature feature2;
	private static Pcm pcm;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		pcm = new Pcm("pcm_test", "test de ValuedCell", "team");
		product1 = new Product("prod1", pcm, null);
		product2 = new Product("prod2", pcm, null);
		feature1 = new Feature("featureDate", new TypeDate(), pcm);
		feature2 = new Feature("featureDouble", new TypeDouble(0, 100, 2), pcm);
		iValuedCell = new ValuedCell(product1, feature1);
		iValuedCell2 = new ValuedCell(product2, feature2);
	}

	@Test
	public void updateValueTrue() {
		assertTrue(iValuedCell.updateValue("29/04/1991"));
		assertTrue(iValuedCell2.updateValue("36"));

	}

	@Test
	public void updateValueFalse() {
		assertFalse(iValuedCell.updateValue("29/04/19911"));
		assertFalse(iValuedCell2.updateValue("56 bonjour"));

	}

	@Test
	public void getIFroduct() {
		assertEquals(product1, iValuedCell.getProduct());
		assertEquals(product2, iValuedCell2.getProduct());
	}

	@Test
	public void getIFeature() {
		assertEquals(feature1, iValuedCell.getFeature());
		assertEquals(feature2, iValuedCell2.getFeature());

	}

}
