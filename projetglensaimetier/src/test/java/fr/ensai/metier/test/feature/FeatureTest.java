package fr.ensai.metier.test.feature;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import fr.ensai.metier.header.Feature;
import fr.ensai.metier.pcm.Pcm;
import fr.ensai.metier.type.TypeBoolean;
import fr.ensai.metier.type.TypeDate;
import fr.ensai.metier.type.TypeString;

public class FeatureTest {

	private Pcm iPcm;
	private Feature feature1;
	private Feature feature2;
	private Feature feature3;
	private Feature feature4;
	private Feature feature5;

	private Set<Feature> childrenFeature1;

	@Before
	public void setUp() throws Exception {
		iPcm = new Pcm("pcmTest", "Feature", "team");
		/**
		 * initialisation des famille (king)
		 */
		feature1 = new Feature("ifeature1", iPcm);
		feature2 = new Feature("ifeature2", iPcm);
		/**
		 * initialisation des features simples
		 */

		feature3 = new Feature("iFeature3", new TypeBoolean(), iPcm);
		feature4 = new Feature("iFeature4", new TypeDate(), iPcm);
		feature5 = new Feature("iFeature5", new TypeString(), iPcm);
		/**
		 * Attribution des enfants
		 */
		childrenFeature1 = new HashSet<Feature>();
		childrenFeature1.add(feature3);
		childrenFeature1.add(feature4);
		feature1.addChild(feature3);
		feature1.addChild(feature4);
		feature2.addChild(feature5);

	}

	@Test
	public void sizeChildren() {
		assertTrue(feature1.getChildren().size() == 2);
		assertTrue(feature2.getChildren().size() == 1);
		assertTrue(feature3.getChildren().size() == 0);
		assertTrue(feature4.getChildren().size() == 0);
		assertTrue(feature5.getChildren().size() == 0);
	}

	@Test
	public void getChildren() {
		Set<Feature> children = new HashSet<Feature>();
		children.add(feature3);
		children.add(feature4);
		assertTrue(feature1.getChildren().containsAll(children));
		assertTrue(feature2.getChildren().contains(feature5));
	}

	@Test
	public void removeChildren() {
		assertTrue(feature1.removeChild(feature3));
		assertFalse(feature1.getChildren().contains(feature3));
	}

	@Test
	public void isChildOf() {
		assertTrue(feature3.isChildOf(feature1));
		assertTrue(feature4.isChildOf(feature1));
		assertTrue(feature5.isChildOf(feature2));
	}

	@Test
	public void getParent() {
		assertTrue(feature3.getParent().equals(feature1));
		assertTrue(feature4.getParent().equals(feature1));
		assertTrue(feature5.getParent().equals(feature2));
	}

	@Test
	public void setParent() {
		feature3.setParent(feature2);
		assertFalse(feature3.getParent().equals(feature1));
		assertTrue(feature3.getParent().equals(feature2));
	}

	@Test
	public void removeParent() {
		feature3.removeParent();

		assertTrue(feature3.getParent() == null);
	}

	@Test
	public void isParentOf() {
		/**
		 * Tests exhaustifs pour la iFeature1
		 */
		assertFalse("iFeature 1 -> iFeature2", feature1.isParentOf(feature2));
		assertTrue("iFeature 1 -> iFeature3", feature1.isParentOf(feature3));
		assertTrue("iFeature 1 -> iFeature4", feature1.isParentOf(feature4));
		assertFalse("iFeature 1 -> iFeature5", feature1.isParentOf(feature5));

		/**
		 * Test partiel pour la iFeature2
		 */
		assertTrue("iFeature 2 -> iFeature5", feature2.isParentOf(feature5));

		/**
		 * Test pour une feature qui n'est pas une famille
		 */
		assertFalse("iFeature3 -> iFeature4", feature3.isParentOf(feature4));
	}

	@Test
	public void isKing() {
		/**
		 * Tests exhaustifs pour la iFeature1
		 */
		assertTrue("iFeature 1", feature1.isKing());
		assertTrue("iFeature 2 ", feature2.isKing());
		assertFalse("iFeature 3 ", feature3.isKing());
		assertFalse("iFeature 4", feature4.isKing());
		assertFalse("iFeature 5", feature5.isKing());
	}

}
