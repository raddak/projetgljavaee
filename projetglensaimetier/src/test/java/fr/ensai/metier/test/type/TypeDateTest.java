package fr.ensai.metier.test.type;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.ensai.metier.type.TypeDate;

public class TypeDateTest {

	// Test avec le constructeur par défaut

	@Test
	public void testVerifConstraintTrue() {
		TypeDate td = new TypeDate();
		assertTrue(td.verifyConstraints("1/2/1990"));
		assertTrue(td.verifyConstraints("10/2/1990"));
	}

	@Test
	public void testVerifConstraintFalse() {
		TypeDate td = new TypeDate();
		// Insertion de lettres
		assertEquals(false, td.verifyConstraints("ab/2/1990"));
		// Faute sur les slash
		assertEquals(false, td.verifyConstraints("22/1990"));
		// Faute sur l'annee
		assertEquals(false, td.verifyConstraints("10/10/1890"));
		// Faute sur le jour negatif
		assertEquals(false, td.verifyConstraints("-0/10/1990"));
		// Faute sur le jour
		assertEquals(false, td.verifyConstraints("36/10/1990"));
		// Faute sur le mois
		assertEquals(false, td.verifyConstraints("1/16/1990"));
	}
	
	// Test avec le constructeur mois

	@Test
	public void testVerifConstraintTrue2() {
		TypeDate td = new TypeDate(1);
		assertTrue("verify 2",td.verifyConstraints("2"));
		assertTrue("verify 02",td.verifyConstraints("02"));
	}

	@Test
	public void testVerifConstraintFalse2() {
		TypeDate td = new TypeDate(1);
		assertFalse("verify 13",td.verifyConstraints("13"));
		assertFalse("verify 0",td.verifyConstraints("0"));
	}
}
