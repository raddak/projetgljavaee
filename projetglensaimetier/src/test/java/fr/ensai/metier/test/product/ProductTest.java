package fr.ensai.metier.test.product;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import fr.ensai.metier.header.Product;
import fr.ensai.metier.pcm.Pcm;

public class ProductTest {

	private Pcm pcm;
	private Product product1;
	private Product product2;
	private Product product3;
	private Product product4;
	private Set<Product> childrenProduct2;

	@Before
	public void setUp() throws Exception {
		pcm = new Pcm("pcmTest", "Feature", "team");
		/**
		 * initialisation des famille (king)
		 */
		product1 = new Product("prod1", pcm, null);
		product2 = new Product("prod2", pcm, product1);
		/**
		 * initialisation des features simples
		 */
		product3 = new Product("prod3", pcm, null);
		/**
		 * Attribution des enfants
		 */
		childrenProduct2 = new HashSet<Product>();
		childrenProduct2.add(product4);
	}

	@Test
	public void getName() {
		assertEquals("prod1", product1.getName());
	}

	@Test
	public void setName() {
		product1.setName("foo");
		assertEquals("foo", product1.getName());
	}

	@Test
	public void getPcm() {
		assertEquals(pcm, product1.getPcm());
	}

	@Test
	public void getChildren() {
		assertTrue(product2.getChildren().size() == 0);
	}

	@Test
	public void setChildren() {
		assertTrue(product2.getChildren().size() == 0);
	}

}
