package fr.ensai.service.test.pcm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.ensai.metier.header.Feature;
import fr.ensai.metier.pcm.Pcm;
import fr.ensai.serialisation.PcmSerializer;
import fr.ensai.service.pcm.PcmService;
import fr.ensai.service.pcm.PcmServiceException;

public class PcmServiceTest {

	// Connection a la base de donnees
	private static EntityManagerFactory EMF;
	private EntityManager em;

	private Pcm pcm;
	private PcmService pcmService;

	private PcmSerializer pcmSerializer;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		EMF = Persistence.createEntityManagerFactory("projetGL-JavaEE");
	}

	@Before
	public void setUp() throws Exception {
		em = EMF.createEntityManager();
		pcmService = new PcmService(em);
		pcmSerializer = new PcmSerializer();
		pcm = importPcm();
		pcmService.create(pcm);
	}

	@After
	public void tearDown() throws Exception {
		pcmService.remove(pcm);
	}

	@Test
	public void create() throws PcmServiceException {
		Query q = em.createQuery("select p from Pcm p");
		assertTrue(q.getResultList().contains(pcm));
	}

	@Test
	public void update1() throws PcmServiceException {
		Pcm pcmPersist = em.find(Pcm.class, pcm.getId());
		pcmPersist.setName("foo");
		pcmPersist = em.find(Pcm.class, pcm.getId());
		assertEquals(pcm, pcmPersist);
	}

	@Test
	public void update2() throws PcmServiceException {
		Pcm pcmPersist = em.find(Pcm.class, pcm.getId());
		pcmPersist.setAuthor("toto");
		pcmPersist = em.find(Pcm.class, pcm.getId());
		assertEquals(pcm, pcmPersist);
	}

	@Test
	public void update3() throws PcmServiceException,
			UnsupportedEncodingException, JAXBException {
		Pcm pcmPersist = em.find(Pcm.class, pcm.getId());
		Feature feature = new Feature("maNouvelleFamilleDeFeature");
		pcmPersist.addFeature(feature);

		Pcm pcmInBase = em.find(Pcm.class, pcm.getId());
		System.err.println(serialize(pcmInBase));
		assertEquals(pcm, pcmInBase);
	}

	// @Test
	public void remove() throws PcmServiceException,
			UnsupportedEncodingException, JAXBException {
		Pcm res = em.find(Pcm.class, pcm.getId());
		assertEquals(pcm, res);
		pcmService.remove(pcm);
		res = em.find(Pcm.class, pcm.getId());
		assertEquals(null, res);
	}

	private Pcm importPcm() {
		try {
			Pcm pcmImport = pcmSerializer.importPcm("monPcm");
			return pcmImport;
		} catch (IOException | JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	private String serialize(Pcm pcm) throws UnsupportedEncodingException,
			JAXBException {
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();

		JAXBContext jaxbContext = JAXBContext.newInstance(Pcm.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.marshal(pcm, outStream);

		String monPcmInXml = outStream.toString("UTF-8");
		return monPcmInXml;
	}
}
