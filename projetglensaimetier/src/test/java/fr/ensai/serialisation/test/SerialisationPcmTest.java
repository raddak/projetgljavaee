package fr.ensai.serialisation.test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.junit.Before;
import org.junit.Test;

import fr.ensai.metier.comment.Commentaire;
import fr.ensai.metier.header.Feature;
import fr.ensai.metier.header.Product;
import fr.ensai.metier.pcm.Pcm;
import fr.ensai.metier.type.TypeBoolean;
import fr.ensai.serialisation.PcmSerializer;

public class SerialisationPcmTest {

	private Pcm pcm;
	private Product product1;
	private Product product2;
	private Feature feature1;
	private Commentaire com;
	private PcmSerializer pcmSerializer;
	private final String FOLDER = "pcm_dispos";

	@Before
	public void setUp() throws Exception {
		pcmSerializer = new PcmSerializer();
		pcm = new Pcm("monPcm", "maDescription", "team");
		product1 = new Product("MonProduit1");
		product2 = new Product("MonProduit2");
		feature1 = new Feature("MaFeatureFamily");
		feature1.addChild(new Feature("maSousFeature1", new TypeBoolean(
				"mon type booleen")));
		product1.addChild(new Product("monSousProd1"));
		com = new Commentaire("foo", "team", new Date());
		pcm.addProduct(product1);
		pcm.addProduct(product2);
		pcm.addFeature(feature1);
		product1.addCommentaire(com);
	}

	@Test
	public void exportPcm() {
		try {
			System.err.println(pcmSerializer.exportPcm(pcm));
		} catch (UnsupportedEncodingException | JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void importPcm() {
		try {
			pcmSerializer.exportPcm(pcm);
			Pcm pcmImport = pcmSerializer.importPcm("monPcm");
			String pcmXml = new String(Files.readAllBytes(Paths.get(FOLDER
					+ "/" + "monPcm.xml")));
			// assertEquals(pcmXml, serialize(pcmImport));
		} catch (IOException | JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String serialize(Pcm pcm) throws UnsupportedEncodingException,
			JAXBException {
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();

		JAXBContext jaxbContext = JAXBContext.newInstance(Pcm.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.marshal(pcm, outStream);

		String monPcmInXml = outStream.toString("UTF-8");
		return monPcmInXml;
	}

}
