package fr.ensai.serialisation.test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.junit.Test;

import fr.ensai.metier.type.Type;
import fr.ensai.metier.type.TypeBoolean;

public class SerialisationTypeTest {
	Type tb;

	@Test
	public void export() throws JAXBException, UnsupportedEncodingException {
		tb = new TypeBoolean("monTypeBooleen");
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		File file = new File("type.xml");
		JAXBContext jaxbContext = JAXBContext.newInstance(Type.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		jaxbMarshaller.marshal(tb, file);
		jaxbMarshaller.marshal(tb, outStream);
		String myTypeInXml = outStream.toString("UTF-8");
		System.err.println(myTypeInXml);
	}

}
