package fr.ensai.serialisation.test;

import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.junit.Before;
import org.junit.Test;

import fr.ensai.metier.comment.Commentaire;
import fr.ensai.metier.header.Product;

public class SerialisationProductTest {

	private Product product;
	private Commentaire com;

	@Before
	public void setUp() throws Exception {
		product = new Product("MonProduit1");
		product.addChild(new Product("monSousProd1"));
		com = new Commentaire("foo", "team", new Date());
		product.addCommentaire(com);
	}

	@Test
	public void exportProduct() {
		System.out.println("SerialisationProductTest.exportProduct()");

		try {
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			File file = new File("product.xml");
			JAXBContext jaxbContext = JAXBContext.newInstance(Product.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			jaxbMarshaller.marshal(product, file);
			jaxbMarshaller.marshal(product, outStream);
			try {
				String monProduitInXml = outStream.toString("UTF-8");
				System.err.println(monProduitInXml);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		} catch (JAXBException e) {
			e.printStackTrace();
		}

	}

	// @Test
	public void importProduct() {
		try {
			// // convert String into InputStream
			// InputStream is = new ByteArrayInputStream(
			// maChaine.getBytes());
			File file = new File("product.xml");

			JAXBContext jaxbContext = JAXBContext.newInstance(Product.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Product importProduct = (Product) jaxbUnmarshaller.unmarshal(file);
			System.err.println(printSerialize(importProduct));

		} catch (JAXBException e) {
			e.printStackTrace();
			fail();
		}
	}

	private String printSerialize(Product product) {
		try {
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();

			JAXBContext jaxbContext = JAXBContext.newInstance(Product.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			jaxbMarshaller.marshal(product, outStream);
			try {
				String maChaine = outStream.toString("UTF-8");
				return maChaine;
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				return null;
			}// Je suppose que ton writer XML utilise l'encodage l'UTF-8
		} catch (JAXBException e) {
			e.printStackTrace();
			return null;
		}
	}
}
