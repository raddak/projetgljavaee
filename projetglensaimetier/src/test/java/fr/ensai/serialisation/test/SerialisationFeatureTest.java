package fr.ensai.serialisation.test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.junit.Before;
import org.junit.Test;

import fr.ensai.metier.header.Feature;
import fr.ensai.metier.type.TypeBoolean;
import fr.ensai.metier.type.TypeDate;
import fr.ensai.metier.type.TypeDouble;
import fr.ensai.metier.type.TypeEnum;
import fr.ensai.metier.type.TypeString;

public class SerialisationFeatureTest {
	Feature feature;
	Feature sousFeature1;
	Feature sousFeature2;
	Feature sousFeature3;
	Feature sousFeature4;
	Feature sousFeature5;

	@Before
	public void setUp() throws Exception {
		feature = new Feature("myFeatureFamily");
		sousFeature1 = new Feature("mySubFeature1", new TypeBoolean(
				"mon type booleen"));
		sousFeature2 = new Feature("mySubFeature2", new TypeString(
				"mon type string"));
		sousFeature3 = new Feature("mySubFeature3", new TypeDate(
				"mon type date"));
		sousFeature4 = new Feature("mySubFeature4", new TypeDouble(0, 100, 2,
				"mon type double"));
		sousFeature5 = new Feature("mySubFeature5", new TypeString(
				"mon type string"));

		feature.addChild(sousFeature1);
		feature.addChild(sousFeature2);
		feature.addChild(sousFeature3);
		feature.addChild(sousFeature4);
		feature.addChild(sousFeature5);
	}

	// @Test
	public void export() throws UnsupportedEncodingException, JAXBException {
		System.out.println("SerialisationFeatureTest.export()");
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		File file = new File("feature.xml");
		JAXBContext jaxbContext = JAXBContext.newInstance(Feature.class,
				TypeDouble.class, TypeString.class, TypeBoolean.class,
				TypeDate.class, TypeEnum.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		jaxbMarshaller.marshal(feature, file);
		jaxbMarshaller.marshal(feature, outStream);
		String myFeatureInXml = outStream.toString("UTF-8");
		System.err.println(myFeatureInXml);
	}

	@Test
	public void importFeature() throws UnsupportedEncodingException,
			JAXBException {
		System.out.println("SerialisationFeatureTest.enclosing_method()");

		File file = new File("feature.xml");

		JAXBContext jaxbContext = JAXBContext.newInstance(Feature.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		Feature importFeature = (Feature) jaxbUnmarshaller.unmarshal(file);
		System.err.println(printSerialize(importFeature));
	}

	private String printSerialize(Feature product) throws JAXBException,
			UnsupportedEncodingException {

		ByteArrayOutputStream outStream = new ByteArrayOutputStream();

		JAXBContext jaxbContext = JAXBContext.newInstance(Feature.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		jaxbMarshaller.marshal(product, outStream);

		String maFeatureSerialisee = outStream.toString("UTF-8");
		return maFeatureSerialisee;
	}

}
