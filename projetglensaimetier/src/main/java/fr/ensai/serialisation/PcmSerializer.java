package fr.ensai.serialisation;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import fr.ensai.metier.pcm.Pcm;
import fr.ensai.metier.type.TypeBoolean;
import fr.ensai.metier.type.TypeDate;
import fr.ensai.metier.type.TypeDouble;
import fr.ensai.metier.type.TypeEnum;
import fr.ensai.metier.type.TypeString;

public class PcmSerializer {

	public PcmSerializer() {
	}

	private final String FOLDER = "pcm_dispos";

	/**
	 * Methode permettant d'exporter un pcm. Cette methode va aussi stocker le
	 * pcm. Le nom du fichier stocke sera le nom du pcm
	 * 
	 * @param pcm
	 *            a exporter.
	 * @throws JAXBException
	 *             cette exception est levee lors d'un probleme avec le
	 *             marshaller
	 * @throws UnsupportedEncodingException
	 *             le fichier xml serialiser sera encode en UTF-8, l'exception
	 *             sera levee en cas d'erreur
	 */
	public String exportPcm(Pcm pcm) throws JAXBException,
			UnsupportedEncodingException {
		File theDirectory = new File(FOLDER);
		try {
			theDirectory.mkdir();
		} catch (SecurityException se) {
			System.err.println(se);
		}

		ByteArrayOutputStream outStream = new ByteArrayOutputStream();

		File file = new File(FOLDER + "/" + pcm.getName() + ".xml");
		JAXBContext jaxbContext;

		jaxbContext = JAXBContext.newInstance(Pcm.class, TypeDouble.class,
				TypeString.class, TypeBoolean.class, TypeDate.class,
				TypeEnum.class);

		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		jaxbMarshaller.marshal(pcm, file);
		jaxbMarshaller.marshal(pcm, outStream);
		return outStream.toString("UTF-8");
	}

	/**
	 * Methode permettant d'instancier un pcm a partir d'un fichier xml
	 * 
	 * @param nomDuPcm
	 *            nom du pcm a importer, il n'y a pas besoin
	 * @return
	 * @throws IOException
	 *             si le nom du fichier n'existe pas
	 * @throws JAXBException
	 *             si il y a un problem jaxb
	 * 
	 */
	public Pcm importPcm(String nomDuPcm) throws IOException, JAXBException {
		String fullName;
		if (nomDuPcm.endsWith(".xml")) {
			fullName = nomDuPcm;
		} else {
			fullName = nomDuPcm + ".xml";
		}
		File file = new File(FOLDER + "/" + fullName);
		JAXBContext jaxbContext = JAXBContext.newInstance(Pcm.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		Pcm importPcm = (Pcm) jaxbUnmarshaller.unmarshal(file);
		return importPcm;
	}

}
