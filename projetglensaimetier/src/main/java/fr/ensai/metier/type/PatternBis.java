package fr.ensai.metier.type;

import java.util.UUID;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@XmlRootElement(name = "pattern")
public class PatternBis {

	/*
	 * Attributs
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String id;
	private Pattern pattern;
	private String description;

	/*
	 * Accesseurs et mutateurs
	 */
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@XmlTransient
	public Pattern getPattern() {
		return pattern;
	}

	public void setPattern(Pattern pattern) {
		this.pattern = pattern;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/*
	 * Constructeurs
	 */
	public PatternBis() {
		this.id = UUID.randomUUID().toString();
	}

	public PatternBis(Pattern pattern, String description) {
		super();
		this.pattern = pattern;
		this.description = description;
	}
}
