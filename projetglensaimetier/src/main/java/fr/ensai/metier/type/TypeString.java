package fr.ensai.metier.type;

import javax.persistence.Entity;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */
@Entity
@XmlRootElement(name = "TypeString")
@XmlAccessorType(XmlAccessType.FIELD)
public class TypeString extends Type {

	/*
	 * Attributs
	 */
	@Transient
	private final int sizeMax = 100;

	/*
	 * Constructeurs
	 */

	/**
	 * Constructeur pour jpa et jaxb
	 */
	public TypeString() {
		super();
	}

	/**
	 * Constructeur utlise pour les pcm
	 * 
	 * @param description
	 */
	public TypeString(String description) {
		super(description);
	}

	/*
	 * Methodes implementees
	 */

	/**
	 * Methode d'initialisation
	 */
	public String create() {
		return new String();
	}

	/**
	 * Methode de verification avant update
	 */
	public boolean verifyConstraints(String value) {
		return value.length() <= this.sizeMax;
	}

	/**
	 * Methode de changement de valeur
	 */
	public String update(String value, String oldValue) {
		oldValue = value;
		return oldValue;
	}

}
