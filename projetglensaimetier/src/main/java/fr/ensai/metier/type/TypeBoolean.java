package fr.ensai.metier.type;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement(name = "TypeBoolean")
public class TypeBoolean extends Type {

	/**
	 * Attributs
	 */
	private Set<String> values = new HashSet<String>();

	/**
	 * Accesseurs et mutateur
	 */
	@XmlElementWrapper(name = "values")
	@XmlElement(name = "item")
	public Set<String> getValues() {
		return values;
	}

	public void setValues(Set<String> values) {
		this.values = values;
	}

	/**
	 * Constructeurs
	 */

	public TypeBoolean() {
		super();
		values.add("true");
		values.add("false");
		values.add("t");
		values.add("f");
		values.add("1");
		values.add("0");
		values.add("");
	}

	public TypeBoolean(String description) {
		super(description);
		values.add("true");
		values.add("false");
		values.add("t");
		values.add("f");
		values.add("1");
		values.add("0");
		values.add("");
	}

	public String create() {
		return "";
	}

	public boolean verifyConstraints(String value) {
		return values.contains(value.toLowerCase());
	}

	public String update(String newValue, String oldValue) {
		oldValue = newValue;
		return oldValue;
	}
}
