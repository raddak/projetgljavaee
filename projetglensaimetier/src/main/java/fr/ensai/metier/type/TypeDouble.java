package fr.ensai.metier.type;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */

@Entity
@XmlRootElement(name = "TypeDouble")
@XmlAccessorType(XmlAccessType.FIELD)
public class TypeDouble extends Type {

	@XmlElement(name = "rangeMin")
	private int rangeMin;
	@XmlElement(name = "rangeMax")
	private int rangeMax;
	@XmlElement(name = "nbDecimal")
	private int nbDecimaleMax;

	/**
	 * Constructeurs
	 */

	/**
	 * Constructeur requis pour jpa et jaxb uniquement.
	 */
	public TypeDouble() {
		super();
	}

	/**
	 * Constructeur utilise pour les tests uniquement
	 * 
	 * @param rangeMin
	 *            borne inferieure
	 * @param rangeMax
	 *            borne superieur
	 * @param nbDecimaleMax
	 *            nombre de chiffres apres la virgule
	 */
	public TypeDouble(int rangeMin, int rangeMax, int nbDecimaleMax) {
		super();
		this.rangeMin = rangeMin;
		this.rangeMax = rangeMax;
		this.nbDecimaleMax = nbDecimaleMax;
	}

	/**
	 * Constructeur utilise pour les pcm
	 * 
	 * @param rangeMin
	 *            borne inferieure
	 * @param rangeMax
	 *            borne superieure
	 * @param nbDecimaleMax
	 *            nombre de chiffre apres la virgule
	 * @param description
	 *            description de la feature
	 */
	public TypeDouble(int rangeMin, int rangeMax, int nbDecimaleMax,
			String description) {
		super(description);
		this.rangeMin = rangeMin;
		this.rangeMax = rangeMax;
		this.nbDecimaleMax = nbDecimaleMax;
	}

	public String create() {
		return new Double(rangeMin).toString();
	}

	public boolean verifyConstraints(String value) {
		try {
			Double test = Double.parseDouble(value);
			return test >= rangeMin && test <= rangeMax;

		} catch (NumberFormatException e) {
			return false;
		}

	}

	public String update(String value, String oldValue) {

		Double nb = Double.parseDouble(value);
		String nbFormatString = format(nb, this.nbDecimaleMax);
		Double nbFormat = Double.parseDouble(nbFormatString);

		return nbFormat.toString();
	}

	private static String format(Number n, int nbDecimalMax) {
		NumberFormat format = DecimalFormat.getInstance(Locale.FRANCE);
		format.setRoundingMode(RoundingMode.FLOOR);
		format.setMinimumFractionDigits(0);
		format.setMaximumFractionDigits(nbDecimalMax);
		return format.format(n).replace(",", ".");
	}

}
