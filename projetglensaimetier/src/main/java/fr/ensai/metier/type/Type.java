package fr.ensai.metier.type;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlSeeAlso;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@MappedSuperclass
@XmlSeeAlso({ TypeBoolean.class, TypeDate.class, TypeDouble.class,
		TypeEnum.class, TypeString.class })
public abstract class Type {

	/**
	 * Attributs
	 */
	@Id
	protected String id;

	protected String description;

	/**
	 * Accesseurs et mutateurs
	 * 
	 */

	@XmlID
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Constructeur
	 */
	public Type() {
	}

	/**
	 * Constructeur utilise pour les pcm
	 * 
	 * @param description
	 */
	public Type(String description) {
		this.id = UUID.randomUUID().toString();
		this.description = description;
	}

	/**
	 * Autres methodes abstraites
	 */
	public abstract String create();

	public abstract boolean verifyConstraints(String value);

	public abstract Object update(String newValue, String oldValue);

}
