package fr.ensai.metier.type;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */

@Entity
@XmlRootElement(name = "TypeEnum")
@XmlAccessorType(XmlAccessType.FIELD)
public class TypeEnum extends Type {

	@XmlElement(name = "values")
	private Set<String> values;

	/**
	 * Constructeurs
	 */

	/**
	 * Constructeurs requis pour jpa et jaxb uniquement
	 */
	public TypeEnum() {
		super();
	}

	/**
	 * constructeur utlise pour les tests uniquement
	 * 
	 * @param values
	 *            item de l'enumerateur
	 */
	public TypeEnum(Set<String> values) {
		super();
		this.values = new HashSet<String>();
		for (String string : values) {
			this.values.add(string.toLowerCase());
		}
	}

	/**
	 * Constructeur utilise pour les pcm
	 * 
	 * @param items
	 *            de l'enumerateur
	 * @param description
	 */
	public TypeEnum(Set<String> items, String description) {
		super(description);
		this.values = new HashSet<String>();
		for (String string : items) {
			this.values.add(string.toLowerCase());
		}
	}

	public String create() {
		return "";
	}

	public boolean verifyConstraints(String value) {
		return values.contains(value.toLowerCase());
	}

	public Object update(String value, String oldvalue) {
		oldvalue = value;
		return oldvalue;
	}

	public Set<String> getValues() {
		return values;
	}
}
