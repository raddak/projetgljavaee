package fr.ensai.metier.type;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */

@Entity
@XmlRootElement(name = "TypeDate")
@XmlAccessorType(XmlAccessType.FIELD)
public class TypeDate extends Type {

	/*
	 * formatter : format des dates pour une feature particulière
	 */
	@OneToMany(mappedBy = "TypeDate")
	private static List<PatternBis> patternsAvailable = patternAvailable();

	private PatternBis pattern;

	/*
	 * Accesseurs et mutateurs
	 */
	public PatternBis getPattern() {
		return pattern;
	}

	public void setPattern(PatternBis pattern) {
		this.pattern = pattern;
	}

	/*
	 * Constructeur par défaut. Pattern="DD/MM/YYYY"
	 */
	public TypeDate() {
		super();
		pattern = patternsAvailable.get(0);
	}

	/*
	 * Constructeur par défaut avec description. Pattern="DD/MM/YYYY"
	 */
	public TypeDate(String description) {
		super(description);
		pattern = patternsAvailable.get(0);
	}

	/**
	 * Constructeur avec choix du format de la date.
	 * 
	 * @param pattern
	 *            : format de la date.
	 */
	public TypeDate(int nbPattern) {
		super();
		if (nbPattern < patternsAvailable.size()) {
			pattern = patternsAvailable.get(nbPattern);
		} else {
			pattern = patternsAvailable.get(0);
		}
	}

	/**
	 * Constructeur avec choix du format de la date.
	 * 
	 * @param pattern
	 *            : format de la date.
	 */
	public TypeDate(int nbPattern, String description) {
		super(description);
		if (nbPattern < patternsAvailable.size()) {
			pattern = patternsAvailable.get(nbPattern);
		} else {
			pattern = patternsAvailable.get(0);
		}
		this.description = description;
	}

	public String create() {
		return (new Date("01/01/2000")).toString();
	}

	public boolean verifyConstraints(String value) {
		Matcher matcher = pattern.getPattern().matcher(value);
		boolean matches = matcher.matches();
		return matches;
	}

	/**
	 * 
	 */
	public Object update(String newValue, String oldValue) {
		oldValue = (new Date(newValue)).toString();
		return oldValue;
	}

	/**
	 * 
	 * @return
	 */
	private static List<PatternBis> patternAvailable() {
		List<PatternBis> patternsAvailable = new ArrayList<PatternBis>();
		String defaultPattern1 = "(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)";
		Pattern pattern1 = Pattern.compile(defaultPattern1);
		String description1 = "DD/MM/YYYY";
		patternsAvailable.add(new PatternBis(pattern1, description1));

		String defaultPattern2 = "((0?[1-9]|1[012]))";
		Pattern pattern2 = Pattern.compile(defaultPattern2);
		String description2 = "MM";
		patternsAvailable.add(new PatternBis(pattern2, description2));

		return patternsAvailable;
	}

}
