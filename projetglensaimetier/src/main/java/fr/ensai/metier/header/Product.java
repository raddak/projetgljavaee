package fr.ensai.metier.header;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import fr.ensai.metier.comment.Commentaire;
import fr.ensai.metier.pcm.Pcm;

@Entity
@XmlRootElement
public class Product extends Header<Product> {

	/*
	 * Attributs
	 */
	@OneToMany(mappedBy = "product", cascade = { CascadeType.ALL })
	private Set<Commentaire> commentaires = new HashSet<Commentaire>();

	/*
	 * Accesseurs et mutateurs + implementation de l'interface
	 */
	// @XmlTransient
	@XmlElementWrapper(name = "commentaires")
	@XmlElement(name = "commentaire")
	public Set<Commentaire> getCommentaires() {
		return commentaires;
	}

	public boolean addCommentaire(Commentaire commentaire) {
		commentaire.setProduct(this);
		return commentaires.add(commentaire);
	}

	/*
	 * Constructeurs
	 */

	public Product() {
		super();
	}

	/**
	 * Constructeur de test pour un produit
	 * 
	 * @param name
	 *            nom du produit
	 * @param pcm
	 *            auquel appartient le produit
	 * @param parent
	 *            famille de produits éventuellement null
	 */
	public Product(String name, Pcm pcm, Header<Product> parent) {
		super();
		this.id = UUID.randomUUID().toString();
		this.name = name;
		this.pcm = pcm;

		if (parent == null) {
			this.isKing = true;
		} else {
			this.isKing = false;
			this.parent = parent;
		}
	}

	/**
	 * Constructeur pour une famille de produits
	 * 
	 * @param name
	 *            nom du produit
	 */
	public Product(String name) {
		super();
		this.id = UUID.randomUUID().toString();
		this.name = name;
		this.isKing = true;
	}

	/**
	 * Constructeur pour un produit appartenant eventuellement a une famille
	 * (attribut parent non null)
	 * 
	 * @param name
	 *            nom du produit
	 * @param parent
	 *            famille de produit, null eventuellement
	 */
	public Product(String name, Header<Product> parent) {
		super();
		this.id = UUID.randomUUID().toString();
		this.name = name;
		if (parent == null) {
			this.isKing = true;
		} else {
			this.isKing = false;
			this.parent = parent;
		}
	}
}
