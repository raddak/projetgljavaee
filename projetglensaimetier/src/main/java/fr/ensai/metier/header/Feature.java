package fr.ensai.metier.header;

import java.util.UUID;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import fr.ensai.metier.pcm.Pcm;
import fr.ensai.metier.type.Type;

@Entity
@XmlRootElement(name = "Feature")
public class Feature extends Header<Feature> {

	/*
	 * Attributs
	 */
	private Type type;

	/**
	 * Accesseurs et mutateurs + surcharge des operateurs usuels de conteneurs
	 */
	@XmlElement(name = "type")
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	/**
	 * Constructeures
	 */
	public Feature() {
		super();
	}

	/**
	 * Constructor for a simple feature for tests
	 * 
	 * @param idHeader
	 * @param nameFeature
	 * @param type
	 * @param pcm
	 */
	public Feature(String nameFeature, Type type, Pcm pcm) {
		super();
		this.id = UUID.randomUUID().toString();
		this.type = type;
		this.pcm = pcm;
		this.name = nameFeature;
		this.isKing = false;
	}

	/**
	 * Constructor for a feature family for tests
	 * 
	 * @param idHeader
	 * @param nameFeature
	 * @param pcm
	 */
	public Feature(String nameFeature, Pcm pcm) {
		super();
		this.id = UUID.randomUUID().toString();
		this.name = nameFeature;
		this.pcm = pcm;
		this.isKing = true;
	}

	/**
	 * Constructor for a feature family
	 * 
	 * @param idHeader
	 * @param nameFeature
	 * @param pcm
	 */
	public Feature(String nameFeature) {
		super();
		this.id = UUID.randomUUID().toString();
		this.name = nameFeature;
		this.isKing = true;
	}

	/**
	 * Constructor for a simple feature
	 * 
	 * @param idHeader
	 * @param nameFeature
	 * @param type
	 * @param pcm
	 */
	public Feature(String nameFeature, Type type) {
		super();
		this.id = UUID.randomUUID().toString();
		this.type = type;
		this.name = nameFeature;
		this.isKing = false;
	}

}
