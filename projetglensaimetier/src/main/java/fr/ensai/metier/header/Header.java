package fr.ensai.metier.header;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;

import fr.ensai.metier.pcm.Pcm;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@MappedSuperclass
public abstract class Header<TypeHeader extends Header<TypeHeader>> {

	/**
	 * Attributs
	 */
	@Id
	protected String id;

	protected String name;

	@ManyToOne(cascade = { CascadeType.ALL })
	protected Pcm pcm;

	protected boolean isKing;

	@OneToMany(targetEntity = Header.class, cascade = { CascadeType.ALL })
	protected Set<TypeHeader> children = new HashSet<TypeHeader>();

	@ManyToOne(targetEntity = Header.class, cascade = { CascadeType.ALL })
	protected Header<TypeHeader> parent;

	/**
	 * Accesseurs et mutateurs + surcharge des operateurs usuels de conteneurs
	 */

	@XmlID
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@XmlElement(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String newName) {
		this.name = newName;
	}

	@XmlIDREF
	public Pcm getPcm() {
		return this.pcm;
	}

	public void setPcm(Pcm pcm) {
		this.pcm = pcm;
	}

	@XmlElementWrapper(name = "children")
	@XmlElement(name = "child")
	public Set<TypeHeader> getChildren() {
		return this.children;
	}

	public int getNbChildren() {
		return this.children.size();
	}

	public void setChildren(Set<TypeHeader> children) {
		if (isKing) {
			this.children.addAll(children);
		} else {
			this.children = new HashSet<TypeHeader>();
			this.children.addAll(children);
		}
	}

	public boolean addChild(TypeHeader child) {
		if (child.getParent() == null) {
			child.setParent(this);
			child.isKing = false;
			child.setPcm(this.pcm);
			return children.add(child);
		} else {
			return false;
		}
	}

	public boolean removeChild(TypeHeader child) {
		child.removeParent();
		return children.remove(child);
	}

	public boolean isChildOf(Header<TypeHeader> header) {
		return this.parent.equals(header);
	}

	@XmlIDREF
	public Header<TypeHeader> getParent() {
		return this.parent;
	}

	public void setParent(Header<TypeHeader> parent) {
		this.parent = parent;
	}

	public void removeParent() {
		this.parent = null;
	}

	public boolean isParentOf(Header<TypeHeader> header) {
		if (this.children != null && this.children.contains(header)) {
			return true;
		} else {
			return false;
		}
	}

	@XmlAttribute
	public boolean isKing() {
		return isKing;
	}

	public void setKing(boolean isKing) {
		this.isKing = isKing;
	}

	/**
	 * Constructeurs
	 */

	public Header() {
	}

}
