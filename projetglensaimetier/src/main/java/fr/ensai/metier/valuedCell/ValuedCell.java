package fr.ensai.metier.valuedCell;

import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;

import fr.ensai.metier.header.Feature;
import fr.ensai.metier.header.Product;
import fr.ensai.metier.pcm.Pcm;
import fr.ensai.metier.type.Type;

/**
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */

@Entity
@XmlRootElement(name = "ValuedCell")
public class ValuedCell {

	/*
	 * Attributs
	 */
	@Id
	private String id;

	@ManyToOne(cascade = CascadeType.PERSIST)
	private Pcm pcm;

	@ManyToOne
	private Product product;

	@ManyToOne
	private Feature feature;

	private String value = "";

	@OneToOne(cascade = { CascadeType.ALL })
	private Type type = null;

	/**
	 * Accesseurs et mutateurs
	 */
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@XmlIDREF
	public Pcm getPcm() {
		return pcm;
	}

	public void setPcm(Pcm pcm) {
		this.pcm = pcm;
	}

	@XmlIDREF
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@XmlIDREF
	public Feature getFeature() {
		return feature;
	}

	public void setFeature(Feature feature) {
		this.feature = feature;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@XmlIDREF
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	/**
	 * Constructeurs
	 */
	public ValuedCell() {
	}

	/**
	 * Constructeur
	 * 
	 * @param product
	 * @param feature
	 */
	public ValuedCell(Product product, Feature feature) {
		this.id = UUID.randomUUID().toString();
		this.feature = feature;
		this.product = product;
		if (feature.getType() != null) {
			this.type = feature.getType();
			this.value = feature.getType().create();
		}

	}

	/**
	 * Constructeur
	 * 
	 * @param product
	 * @param feature
	 */
	public ValuedCell(Product product, Feature feature, Pcm pcm) {
		this.id = UUID.randomUUID().toString();
		this.feature = feature;
		this.product = product;
		this.pcm = pcm;
		if (feature.getType() != null) {
			this.type = feature.getType();
			this.value = feature.getType().create();
		}

	}

	/**
	 * Change the value of the cell
	 * 
	 * @param value
	 *            to update, the format of value will be verified
	 * @return true if the value has been updated correctly
	 */
	public boolean updateValue(String value) {
		if (checkType(value)) {
			this.value = value;
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Private method to know if the new value can replace the current
	 * 
	 * @param value
	 *            new value for the cell
	 * @return true if the new value has the right format
	 */
	private boolean checkType(String value) {
		return type.verifyConstraints(value);
	}

}
