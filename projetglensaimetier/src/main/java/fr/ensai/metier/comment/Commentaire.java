package fr.ensai.metier.comment;

import java.util.Date;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;

import fr.ensai.metier.header.Product;

@Entity
@XmlRootElement(name = "Commentaire")
public class Commentaire {

	/**
	 * Attributs
	 */
	@Id
	private String id;

	private String contenu;

	private String auteur;

	@Temporal(TemporalType.DATE)
	private Date date;

	@ManyToOne(cascade = { CascadeType.ALL })
	private Product product;

	/**
	 * Accesseurs et mutateurs
	 */

	@XmlElement
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@XmlElement
	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	@XmlElement
	public String getAuteur() {
		return auteur;
	}

	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}

	@XmlElement
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@XmlIDREF
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	/**
	 * Constructeurs
	 */

	public Commentaire() {
		super();
		// this.id = UUID.randomUUID().toString();
	}

	/**
	 * Constructeurs utilisant tous les attributs
	 * 
	 * @param contenu
	 * @param auteur
	 * @param date
	 * @param product
	 */
	public Commentaire(String contenu, String auteur, Date date, Product product) {
		super();
		this.id = UUID.randomUUID().toString();
		this.contenu = contenu;
		this.auteur = auteur;
		this.date = date;
		this.product = product;
	}

	/**
	 * 
	 * @param commentaire
	 * @param auteur
	 * @param date
	 */
	public Commentaire(String commentaire, String auteur, Date date) {
		super();
		this.id = UUID.randomUUID().toString();
		this.contenu = commentaire;
		this.auteur = auteur;
		this.date = date;
	}

}
