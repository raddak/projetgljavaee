package fr.ensai.metier.pcm;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;

import fr.ensai.metier.header.Feature;
import fr.ensai.metier.header.Product;
import fr.ensai.metier.valuedCell.ValuedCell;

@Entity
@XmlRootElement(name = "Pcm")
public class Pcm {

	/*
	 * Attributs indispensables a la construction du pcm
	 */
	@Id
	private String id;

	private String name;

	private String description;

	private String author;

	@Temporal(TemporalType.DATE)
	private Date dateCreation;

	@Temporal(TemporalType.DATE)
	private Date dateDerniereModif;

	/*
	 * Composants du pcm
	 */

	@OneToMany(mappedBy = "Pcm", targetEntity = Product.class, cascade = { CascadeType.ALL })
	private Set<Product> products = new HashSet<Product>();

	@OneToMany(mappedBy = "Pcm", targetEntity = Feature.class, cascade = { CascadeType.ALL })
	private Set<Feature> features = new HashSet<Feature>();

	@OneToMany(mappedBy = "pcm", cascade = { CascadeType.ALL })
	private Set<ValuedCell> valuedCells = new HashSet<ValuedCell>();

	/*
	 * Accesseurs et mutateurs
	 */

	@XmlID
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@XmlElement(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@XmlElement(name = "author")
	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	@XmlElement(name = "dateDerniereModif")
	public Date getDateDerniereModif() {
		return dateDerniereModif;
	}

	public void setDateDerniereModif(Date dateDerniereModif) {
		this.dateDerniereModif = dateDerniereModif;
	}

	@XmlElement(name = "dateCreation")
	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	@XmlElementWrapper(name = "products")
	@XmlElement(name = "product")
	public Set<Product> getProducts() {
		return products;
	}

	public void setProducts(Set<Product> products) {
		this.products = products;
	}

	@XmlElementWrapper(name = "features")
	@XmlElement(name = "feature")
	public Set<Feature> getFeatures() {
		return features;
	}

	public void setFeatures(Set<Feature> features) {
		this.features = features;
	}

	@XmlElementWrapper(name = "valuedCells")
	@XmlElement(name = "valuedCell")
	public Set<ValuedCell> getValuedCells() {
		return valuedCells;
	}

	public void setValuedCells(Set<ValuedCell> valuedCells) {
		this.valuedCells = valuedCells;
	}

	public int getNbValuedCells() {
		return this.valuedCells.size();
	}

	/**
	 * Constructeurs
	 */

	public Pcm() {
		this.dateCreation = new Date();
		this.dateDerniereModif = this.dateCreation;
	}

	/**
	 * Constructor
	 * 
	 * @param name
	 * @param description
	 * @param author
	 */
	public Pcm(String name, String description, String author) {
		this.id = UUID.randomUUID().toString();
		this.name = name;
		this.description = description;
		this.author = author;
		this.dateCreation = new Date();
		this.dateDerniereModif = this.dateCreation;
	}

	/*
	 * Methodes fonctionnelles
	 */

	/**
	 * Methode permettant d'ajouter un produit ou une famille de produit au pcm.
	 * Cette methode met a jour la date de modification du PCM, elle attribue le
	 * pcm au produit.
	 * 
	 * @param product
	 * @return True si le produit a ete correctement ajoute
	 */
	public boolean addProduct(Product product) {
		if (this.products.add(product)) {
			this.dateDerniereModif = new Date();
			product.setPcm(this);
			if (product.getNbChildren() == 0) {
				for (Feature feature : features) {
					ValuedCell cell = new ValuedCell(product, feature, this);
					valuedCells.add(cell);
				}
			} else {
				for (Product prod : product.getChildren()) {
					for (Feature feature : features) {
						ValuedCell cell = new ValuedCell(prod, feature, this);
						valuedCells.add(cell);
					}
				}
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Methode permettant de supprimer un produit. Cette methode va aussi
	 * supprimer les valuedCells correspondant au produit en question. La
	 * methode empeche la suppresion de famille de produits
	 * 
	 * @param product
	 *            a supprimer
	 * @return true si le produit a ete correctement supprimer
	 */
	public boolean removeProduct(Product product) {
		if (product.getNbChildren() == 0 && this.features.remove(product)) {
			this.dateDerniereModif = new Date();
			List<ValuedCell> temp = getValuedCellsProduct(product);
			this.valuedCells.removeAll(temp);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Methode pour ajouter une feature ou une famille de feature au PCM. Cette
	 * methode s'occupe aussi de d'ajouter les valuedCells. Cette methode met a
	 * jour la date de derniere modification du pcm et attribue le pcm a la
	 * feature
	 * 
	 * @param feature
	 *            ajouter
	 * @return true si la feature a ete ajoutee avec succes
	 */
	public boolean addFeature(Feature feature) {
		if (this.features.add(feature)) {
			this.dateDerniereModif = new Date();
			feature.setPcm(this);
			if (feature.getNbChildren() == 0) {
				for (Product product : products) {
					ValuedCell cell = new ValuedCell(product, feature, this);
					valuedCells.add(cell);
				}
			} else {
				for (Feature feat : feature.getChildren()) {
					for (Product product : products) {
						ValuedCell cell = new ValuedCell(product, feat, this);
						valuedCells.add(cell);
					}
				}
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Methode permettant de supprimer une feature du pcm. La methode s'occupe
	 * aussi de supprimer les cellules affiliees a ce produit. Si la feature a
	 * ete correctement supprimee, la methode fait aussi un update de la date de
	 * derniere modification
	 * 
	 * @param feature
	 *            a supprimer
	 * @return True si la feature a ete correctement supprimee
	 */
	public boolean removeFeature(Feature feature) {
		if (this.features.remove(feature)) {
			this.dateDerniereModif = new Date();
			List<ValuedCell> temp = getValuedCellsFeature(feature);
			this.valuedCells.removeAll(temp);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Methode permettant de
	 */
	public List<ValuedCell> getValuedCellsProduct(Product product) {
		List<ValuedCell> valuedCellsProduct = new ArrayList<ValuedCell>();
		for (ValuedCell valuedCell : valuedCells) {
			if (valuedCell.getProduct().equals(product)) {
				valuedCellsProduct.add(valuedCell);
			}
		}
		return valuedCellsProduct;
	}

	/**
	 * Methode permettant d'obtenir toutes les valuedCells correspondant a une
	 * feature
	 */
	public List<ValuedCell> getValuedCellsFeature(Feature feature) {
		List<ValuedCell> valuedCellsFeature = new ArrayList<ValuedCell>();
		for (ValuedCell valuedCell : valuedCells) {
			if (valuedCell.getProduct().equals(feature)) {
				valuedCellsFeature.add(valuedCell);
			}
		}
		return valuedCellsFeature;
	}

	/**
	 * Methode pour obtenir une valuedCell a partir d'un produit et d'un feature
	 * 
	 * @param feature
	 * @param product
	 * @return la valuedCell correspondante. La valeur vaut null si aucune
	 *         valuedCell n'a ete trouvee
	 */
	public ValuedCell getValuedCellsFeature(Feature feature, Product product) {
		ValuedCell ValuedCellFeatureProduct = null;
		for (ValuedCell icell : valuedCells) {
			if (icell.getProduct().equals(product)
					&& icell.getFeature().equals(feature)) {

				ValuedCellFeatureProduct = icell;
			}
		}
		return ValuedCellFeatureProduct;
	}
}
