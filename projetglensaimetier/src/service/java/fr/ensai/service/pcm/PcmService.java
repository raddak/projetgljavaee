package fr.ensai.service.pcm;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import fr.ensai.metier.pcm.Pcm;

public class PcmService {

	private EntityManagerFactory emf;

	private EntityManager em;

	private PcmServiceEJB pcmServiceEJB;

	public PcmService() {
		this.emf = Persistence.createEntityManagerFactory("projetGL-JavaEE");
		this.em = emf.createEntityManager();
		pcmServiceEJB = new PcmServiceEJB(em);
	}

	public PcmService(EntityManager em) {
		this.em = em;
		pcmServiceEJB = new PcmServiceEJB(em);
	}

	public void create(Pcm pcm) {
		em.getTransaction().begin();
		pcmServiceEJB.create(pcm);
		em.getTransaction().commit();
	}

	public void update(Pcm pcm) throws PcmServiceException {
		em.getTransaction().begin();
		try {
			pcmServiceEJB.update(pcm);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new PcmServiceException(e);
		}
	}

	public void remove(Pcm pcm) throws PcmServiceException {
		em.getTransaction().begin();
		pcmServiceEJB.remove(pcm);
		em.getTransaction().commit();
	}
}
