package fr.ensai.service.pcm;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fr.ensai.metier.pcm.Pcm;

@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class PcmServiceEJB {

	@PersistenceContext(name = "projetGL-JavaEE")
	private EntityManager em;

	public PcmServiceEJB() {
	}

	public PcmServiceEJB(EntityManager em) {
		this.em = em;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void create(Pcm pcm) {
		em.persist(pcm);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void update(Pcm pcm) throws PcmServiceException {
		Pcm pcmPersist = em.find(Pcm.class, pcm.getId());
		if (pcmPersist == null) {
			throw new PcmServiceException("le pcm est inconnu en base");
		} else {
			pcmPersist = pcm;
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void remove(Pcm pcm) throws PcmServiceException {
		Pcm pcmPersist = em.find(Pcm.class, pcm.getId());
		if (pcmPersist == null) {
			throw new PcmServiceException("le pcm est inconnu en base");
		} else {
			em.remove(pcmPersist);
		}
	}
}
