package fr.ensai.service.pcm;

public class PcmServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PcmServiceException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public PcmServiceException(Exception e) {
		super(e);
		// TODO Auto-generated constructor stub
	}
}
